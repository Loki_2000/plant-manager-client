## Plant Manager

**First version of the Plant Manager application(client-side) written in Backbone.js and Marionette.js.** 

The concept of this application is to allow "Plant Yard" staff to receive collection and delivery orders that are made via site-manager. The application allows users to assign 
themselves an order and change the order-status when delivery / collection has been complete.

Also See: site-manager-client.

---

## Steps to Setup Locally

**NOTE: Currently the application runs on node v6.9.1. Errors may occur on other versions. (*Needs to be updated to v8.X*)**


1. Install NVM ```curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.6/install.sh | bash```

2. Install correct version of node ```nvm install 6.9.1 && nvm use 6.9.1```

1. Clone Repositiory 

2. Navigate to Repository

3. Run ```npm install```

4. Ensure gulp is installed globally. ```npm install -g gulp```

5. Run ```gulp develop```

*Server should run on port 3000*

*NOTE: Default API URL is "htp://localhost:8081/api" - For this to work you must have site-manager-srv installed locally and running* 